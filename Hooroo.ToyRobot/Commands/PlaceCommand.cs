﻿using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Commands
{
    public class PlaceCommand : ICommand
    {
        public PlaceCommand(int x, int y, Direction facing)
        {
            this.X = x;
            this.Y = y;
            this.Facing = facing;
        }

        public Direction Facing { get; }
        public int X { get; }
        public int Y { get; }
    }

    public enum Direction
    {
        EAST,
        WEST,
        NORTH,
        SOUTH
    }
}
