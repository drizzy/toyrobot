﻿using Hooroo.ToyRobot.CommandHandlers;
using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using Hooroo.ToyRobot.Interfaces;
using Hooroo.ToyRobot.Parsers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot
{
    public class ToyRobot
    {
        private readonly ICommandHandlerFactory commandHandleFactory;        
        private readonly ICommandReader commandReader;        

        private State state;

        // TODO: Wire-up IOC
        //public ToyRobot(ICommandHandlerFactory commandHandlerFactory, ICommandReader commandReader, ITextOutput textOutput)
        //{
        //    this.commandHandleFactory = commandHandlerFactory;
        //    this.commandReader = commandReader;
        //    this.textOutput = textOutput;            
        //}

        // TODO: Wire-up DI container
        public ToyRobot(ITextOutput textOutput)
        {
            this.commandReader = new CommandReader(
                   new ICommandParser[]
                   {
                        new LeftCommandParser(),
                        new RightCommandParser(),
                        new MoveCommandParser(),
                        new ReportCommandParser(),
                        new PlaceCommandParser(),
                   }, textOutput);

           this.commandHandleFactory = new PseudoStaticCommandHandlerFactory(new ICommandHandler[]
            {
                new MoveCommandHandler(),
                new RightCommandHandler(),
                new LeftCommandHandler(),
                new ReportCommandHandler(textOutput),
                new PlaceCommandHandler()
            });


            this.state = new State()
            {
                Position = null,
                Table = new ZeroBoundedCartesianTable(5, 5)
            };
        }

        public void ProcessCommands(string command)
        {
            foreach (var commandObj in commandReader.GetCommands(command))
            {
                // Don't do anything until we have a valid position, set by the PlaceCommand
                if (state.Position != null || commandObj.GetType() == typeof(PlaceCommand))
                {
                    var handler = commandHandleFactory.GetCommandHandler(commandObj);
                    this.state = handler.Process(state, commandObj);
                }
            }
        }
    }
}
