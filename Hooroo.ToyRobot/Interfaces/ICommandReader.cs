﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Interfaces
{
    public interface ICommandReader 
    {
        IEnumerable<ICommand> GetCommands(string input);        
    }
}
