﻿namespace Hooroo.ToyRobot.Interfaces
{
    public interface ITextOutput
    {
        void WriteLine(string output);
    }
}