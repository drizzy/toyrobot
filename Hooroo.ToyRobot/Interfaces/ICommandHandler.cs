﻿using Hooroo.ToyRobot.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Interfaces
{
    public interface ICommandHandler<T> where T : ICommand
    {
        State Process(State state, T command);
    }

    public interface ICommandHandler
    {
        State Process(State state, ICommand command);
    }
}
