﻿namespace Hooroo.ToyRobot.Interfaces
{
    public interface ICommandParser
    {
        bool Handles(string command);
        ICommand Parse(string command);
    }

    public interface ICommandParser<T> where T : ICommand
    {
        bool Handles(string command);
        T Parse(string command);
    }
}
