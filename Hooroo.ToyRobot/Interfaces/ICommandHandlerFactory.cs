﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Interfaces
{
    public interface ICommandHandlerFactory
    {
        ICommandHandler GetCommandHandler(ICommand command);
    }
}
