﻿using Hooroo.ToyRobot.Commands;

namespace Hooroo.ToyRobot.Game.Interfaces
{
    public interface ITable
    {
        bool IsValidPosition(int x, int y);
        (int X, int Y) CalculateNextPosition(int currentX, int currentY, Direction direction);
    }
}