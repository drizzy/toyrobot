﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Parsers
{
    public class MoveCommandParser : VeryBasicCommandParser<MoveCommand>
    {
        public MoveCommandParser() : base("MOVE") { }
    }
}
