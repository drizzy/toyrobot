﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Parsers
{
    public class RightCommandParser : VeryBasicCommandParser<RightCommand>
    {
        public RightCommandParser() : base("RIGHT")
        {
        }
    }

}
