﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Parsers
{
    public class PlaceCommandParser : ICommandParser<PlaceCommand>, ICommandParser
    {
        public bool Handles(string command)
        {
            var handles = (command?.StartsWith("PLACE"));
            return (handles.HasValue && handles.Value);
        }

        public PlaceCommand Parse(string command)
        {
            // Guard clauses
            if (string.IsNullOrEmpty(command))
                throw new ArgumentNullException(nameof(command));                    
            if (!command.StartsWith("PLACE", StringComparison.Ordinal))
                throw new ArgumentException($"{command} is not a valid command");

            var parameterGroup = command.Split(' ');
            if (parameterGroup.Length != 2)
                throw new ArgumentException($"{command} is not a valid command");

            var parameters = parameterGroup[1].Split(',');
            if (parameters.Length != 3)
                throw new ArgumentException($"{command} is not a valid command");

            if (!int.TryParse(parameters[0], out int x))
                throw new ArgumentException($"Value {parameters[0]} not valid for X");
            if (!int.TryParse(parameters[1], out int y))
                throw new ArgumentException($"Value {parameters[1]} not valid for X");
            if (!Enum.TryParse(typeof(Direction), parameters[2], out object facing))
                throw new ArgumentException($"Value {parameters[2]} is not a valid direction");

            return new PlaceCommand(x, y, (Direction)facing);
        }

        ICommand ICommandParser.Parse(string command)
        {
            return this.Parse(command);
        }
    }
}
