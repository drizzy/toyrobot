﻿using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Parsers
{
    public abstract class VeryBasicCommandParser<T> : ICommandParser, ICommandParser<T> where T : class, ICommand, new()
    {
        private readonly string commandName;

        public VeryBasicCommandParser(string commandName)
        {
            this.commandName = commandName;
        }

        public bool Handles(string command) => string.Equals(command, commandName, StringComparison.Ordinal);
        public T Parse(string command) => Handles(command) ? new T() : throw new ArgumentException($"{command} is not a valid command");

        ICommand ICommandParser.Parse(string command)
        {
            return this.Parse(command);
        }
    }
}
