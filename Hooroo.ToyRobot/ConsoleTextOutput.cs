﻿using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot
{
    public class ConsoleTextOutput : ITextOutput
    {
        public void WriteLine(string output)
        {
            Console.WriteLine(output);
        }
    }
}
