﻿using Hooroo.ToyRobot.Game.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Game
{
    public class State
    {
        public Position Position { get; set; }
        public ITable Table { get; set; }
    }
}
