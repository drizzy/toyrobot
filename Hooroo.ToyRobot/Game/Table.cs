﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Game
{
    public class ZeroBoundedCartesianTable : ITable
    {
        private static Dictionary<Direction, Func<int, int, (int x, int y)>> positionFuncMap = new Dictionary<Direction, Func<int, int, (int x, int y)>>
        {
            {Direction.EAST, (x,y) => (x+1, y) },
            {Direction.WEST, (x,y) => (x-1, y) },
            {Direction.NORTH, (x,y) => (x, y+1) },
            {Direction.SOUTH, (x,y) => (x, y-1) },
        };

        public ZeroBoundedCartesianTable(int maxX, int maxY)
        {
            this.MaxX = maxX;
            this.MaxY = maxY;
        }

        public int MaxX { get; }
        public int MaxY { get; }

        public (int X, int Y) CalculateNextPosition(int currentX, int currentY, Direction direction) =>
            positionFuncMap[direction](currentX, currentY);

        public bool IsValidPosition(int x, int y) => x >= 0 && x <= MaxX && y >= 0 && y <= MaxY;
        
    }
}
