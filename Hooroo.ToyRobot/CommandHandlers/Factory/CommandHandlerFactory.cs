﻿using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hooroo.ToyRobot
{
    public class PseudoStaticCommandHandlerFactory : ICommandHandlerFactory
    {
        private readonly IEnumerable<ICommandHandler> handlers;
        private Dictionary<Type, ICommandHandler> handlerMap = new Dictionary<Type, ICommandHandler>();

        public PseudoStaticCommandHandlerFactory(IEnumerable<ICommandHandler> commandHandlers)
        {
            this.handlers = commandHandlers;
            foreach (var handler in commandHandlers)
            {
                // Get generic type
                var genericHandlerType = handler.GetType().GetInterfaces().FirstOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICommandHandler<>));

                if (genericHandlerType != null)
                {
                    var genericType = genericHandlerType.GenericTypeArguments.FirstOrDefault();

                    // Add to dictionary
                    handlerMap.Add(genericType, handler);
                }
            }            
        }

        public ICommandHandler GetCommandHandler(ICommand command)
        {
            if (handlerMap.TryGetValue(command.GetType(), out ICommandHandler commandHandler))
            {
                return commandHandler;
            }
            return null;
        }
    }
}
