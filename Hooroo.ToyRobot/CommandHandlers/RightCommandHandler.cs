﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.CommandHandlers
{
    public class RightCommandHandler : BaseCommandHandler<RightCommand>
    {
        private static Dictionary<Direction, Direction> directionMap = new Dictionary<Direction, Direction>
        {
            { Direction.EAST, Direction.SOUTH },
            { Direction.SOUTH, Direction.WEST },
            { Direction.WEST, Direction.NORTH },
            { Direction.NORTH, Direction.EAST },
        };

        public override State Process(State state, RightCommand command)
        {
            var newDirection = directionMap[state.Position.Facing];
            return new State
            {
                Position = new Position(state.Position.X, state.Position.Y, newDirection),
                Table = state.Table
            };
        }
    }
}
