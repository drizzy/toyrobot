﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.CommandHandlers
{
    public class ReportCommandHandler : BaseCommandHandler<ReportCommand>
    {
        private readonly ITextOutput textOutput;

        public ReportCommandHandler(ITextOutput textOutput)
        {
            this.textOutput = textOutput;
        }

        public override State Process(State state, ReportCommand command)
        {
            if (state.Position != null)
            {
                textOutput.WriteLine($"> {state.Position.X},{state.Position.Y},{state.Position.Facing.ToString()}");
            }
            return state;
        }
    }
}
