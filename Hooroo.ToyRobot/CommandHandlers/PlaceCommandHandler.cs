﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.CommandHandlers
{
    public class PlaceCommandHandler : BaseCommandHandler<PlaceCommand>
    {        
        public override State Process(State state, PlaceCommand command)
        {
            if (state.Table.IsValidPosition(command.X, command.Y))
            {
                return new State {
                    Position = new Game.Position(command.X, command.Y, command.Facing),
                    Table = state.Table
                };
            }
            return state;
        }
    }
}
