﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.CommandHandlers
{
    public class LeftCommandHandler : BaseCommandHandler<LeftCommand>
    {
        private static IReadOnlyDictionary<Direction, Direction> directionMap = new Dictionary<Direction, Direction>
        {
            { Direction.EAST, Direction.NORTH },
            { Direction.NORTH, Direction.WEST },
            { Direction.WEST, Direction.SOUTH },
            { Direction.SOUTH, Direction.EAST },
        };

        public override State Process(State state, LeftCommand command) =>
            new State
            {
                Position = new Position(state.Position.X, state.Position.Y, directionMap[state.Position.Facing]),
                Table = state.Table
            };
    }

}
