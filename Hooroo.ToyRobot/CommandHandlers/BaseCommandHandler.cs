﻿using Hooroo.ToyRobot.Game;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.CommandHandlers
{
    public abstract class BaseCommandHandler<T> : ICommandHandler<T>, ICommandHandler where T : ICommand
    {
        public abstract State Process(State state, T command);

        public State Process(State state, ICommand command)
        {
            return this.Process(state, (T)command);
        }
    }
}
