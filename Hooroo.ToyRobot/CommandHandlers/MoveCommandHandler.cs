﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.CommandHandlers
{
    public class MoveCommandHandler : BaseCommandHandler<MoveCommand>
    {
        public override State Process(State state, MoveCommand command)
        {
            var (X, Y) = state.Table.CalculateNextPosition(state.Position.X, state.Position.Y, state.Position.Facing);
            if (state.Table.IsValidPosition(X, Y))
            {
                return new State
                {
                    Table = state.Table,
                    Position = new Position(X, Y, state.Position.Facing)
                };
            }
            return state;
        }
    }
}
