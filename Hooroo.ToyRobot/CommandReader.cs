﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Hooroo.ToyRobot
{
    public class CommandReader : ICommandReader
    {
        private readonly IEnumerable<ICommandParser> parsers;
        private readonly ITextOutput textOutput;        

        public CommandReader(IEnumerable<ICommandParser> parsers, ITextOutput textOutput)
        {
            this.parsers = parsers;
            this.textOutput = textOutput;
        }

        public IEnumerable<ICommand> GetCommands(string commands)
        {
            var commandSplit = commands.Split(Environment.NewLine);
            foreach (var command in commandSplit)
            {
                var parser = parsers.FirstOrDefault(p => p.Handles(command));                
                if (parser != null)
                {
                    ICommand parsedCommand = null;
                    try
                    {
                        parsedCommand = parser.Parse(command);
                    }
                    catch (Exception ex)
                    {
                        textOutput.WriteLine(ex.Message);
                        break;
                    }

                    yield return parsedCommand;                    
                }
            }
          
        }
    }
}
