﻿using Hooroo.ToyRobot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hooroo.ToyRobot.Tests.Mocks
{
    public class MockTestOutput : ITextOutput
    {
        public List<string> Output { get; set; } = new List<string>();

        public void WriteLine(string output)
        {
            Output.Add(output);
        }
    }
}
