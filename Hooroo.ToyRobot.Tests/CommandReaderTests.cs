﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using Hooroo.ToyRobot.Parsers;
using Hooroo.ToyRobot.Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Hooroo.ToyRobot.Tests.CommandHandlerFactoryTests;

namespace Hooroo.ToyRobot.Tests
{
    public class CommandReaderTests
    {
        private readonly MockTestOutput textOutput;
        private readonly CommandReader target;
        
        public CommandReaderTests(ITestOutputHelper testOutput)
        {
            this.textOutput = new MockTestOutput();
            this.target = new CommandReader(
                    new ICommandParser[]
                    {
                        new LeftCommandParser(),
                        new RightCommandParser(),
                        new MoveCommandParser(),
                        new ReportCommandParser(),
                        new PlaceCommandParser(),
                    }, textOutput);
        }

        [Fact]
        public void ReadCommands_Returns_Correct_NumberOfComments()
        {
            // arrange
            string input = @"
PLACE 1,1,WEST
RIGHT
REPORT";

            // act 
            var commands = target.GetCommands(input).ToList();

            // assert
            Assert.Equal(3, commands.Count);
        }

        [Fact]
        public void ReadCommands_Returns_Commands_InSequence()
        {
            // arrange
            string input = @"PLACE 1,2,EAST
MOVE
LEFT
RIGHT
REPORT";

            // act 
            var commands = target.GetCommands(input).ToList();

            // assert
            Assert.Equal(5, commands.Count);
            Assert.IsType<PlaceCommand>(commands[0]);
            Assert.IsType<MoveCommand>(commands[1]);
            Assert.IsType<LeftCommand>(commands[2]);
            Assert.IsType<RightCommand>(commands[3]);
            Assert.IsType<ReportCommand>(commands[4]);
        }
    }
}
