﻿using Hooroo.ToyRobot.CommandHandlers;
using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Hooroo.ToyRobot.Tests.CommandHandlers
{
    public class PlaceCommandHandlerTests
    {
        private readonly PlaceCommandHandler target;

        public PlaceCommandHandlerTests()
        {
            this.target = new PlaceCommandHandler();
        }

        [Theory]
        [InlineData(1, 1, Direction.EAST)]
        [InlineData(2, 4, Direction.EAST)]
        [InlineData(5, 5, Direction.NORTH)]
        [InlineData(4, 4, Direction.SOUTH)]
        public void Valid_Targets_Succeed(int x, int y, Direction direction)
        {
            // arrange
            var command = new PlaceCommand(x, y, direction);
            var state = new State { Position = null, Table = new ZeroBoundedCartesianTable(5, 5) };

            // act
            state = target.Process(state, command);

            // assert
            Assert.Equal(x, state.Position.X);
            Assert.Equal(y, state.Position.Y);
            Assert.Equal(direction, state.Position.Facing);
        }

        [Theory]
        [InlineData(-1, 1, Direction.EAST)]
        [InlineData(55, 4, Direction.EAST)]
        [InlineData(5, 55, Direction.NORTH)]
        [InlineData(4, -2, Direction.SOUTH)]
        public void Invalid_Targets_Do_Not_Set_State(int x, int y, Direction direction)
        {
            // arrange
            var command = new PlaceCommand(x, y, direction);
            var state = new State { Position = null, Table = new ZeroBoundedCartesianTable(5, 5) };

            // act
            state = target.Process(state, command);

            // assert
            Assert.Null(state.Position);
        }
    }
}
