﻿using Hooroo.ToyRobot.CommandHandlers;
using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Hooroo.ToyRobot.Tests.CommandHandlers
{
    public class MoveCommandHandlerTests
    {
        private readonly MoveCommandHandler target;

        public MoveCommandHandlerTests()
        {
            this.target = new MoveCommandHandler();
        }

        [Theory]
        [InlineData(0, 0, Direction.NORTH, 0, 1)]
        [InlineData(2, 2, Direction.NORTH, 2, 3)]
        [InlineData(2, 2, Direction.SOUTH, 2, 1)]
        [InlineData(2, 2, Direction.EAST, 3, 2)]
        [InlineData(2, 2, Direction.WEST, 1, 2)]
        public void Move_Supports_All_Directions(int initialX, int initialY, Direction direction, int expectedX, int expectedY)
        {
            // arrange
            var state = new State()
            {
                Position = new Position(initialX, initialY, direction),
                Table = new ZeroBoundedCartesianTable(5, 5)
            };

            // act
            state = target.Process(state, new MoveCommand());

            // assert
            Assert.Equal(expectedX, state.Position.X);
            Assert.Equal(expectedY, state.Position.Y);

        }

        [Theory]
        [InlineData(0, 0, Direction.WEST, 0, 0)]
        [InlineData(5, 5, Direction.NORTH, 5, 5)]
        [InlineData(3, 0, Direction.SOUTH, 3, 0)]
        [InlineData(5, 2, Direction.EAST, 5, 2)]        
        public void Move_Respects_Boundaries(int initialX, int initialY, Direction direction, int expectedX, int expectedY)
        {
            // arrange
            var state = new State()
            {
                Position = new Position(initialX, initialY, direction),
                Table = new ZeroBoundedCartesianTable(5, 5)
            };

            // act
            state = target.Process(state, new MoveCommand());

            // assert
            Assert.Equal(expectedX, state.Position.X);
            Assert.Equal(expectedY, state.Position.Y);

        }
    }
}
