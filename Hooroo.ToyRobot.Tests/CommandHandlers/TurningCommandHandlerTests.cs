﻿using Hooroo.ToyRobot.CommandHandlers;
using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Game;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Hooroo.ToyRobot.Tests
{
    public class TurningCommandHandlerTests
    {
        private readonly LeftCommandHandler leftTarget;
        private readonly RightCommandHandler rightTarget;

        public TurningCommandHandlerTests()
        {
            this.leftTarget = new LeftCommandHandler();
            this.rightTarget = new RightCommandHandler();
        }

        [Theory]
        [InlineData(Direction.NORTH, Direction.WEST)]
        [InlineData(Direction.WEST, Direction.SOUTH)]
        [InlineData(Direction.SOUTH, Direction.EAST)]
        [InlineData(Direction.EAST, Direction.NORTH)]
        public void Left_Command_Correctly_Changes_Direction(Direction currentDirection, Direction expected)
        {
            // arrange
            var state = new State
            {
                Position = new Position(1, 1, currentDirection),
                Table = new ZeroBoundedCartesianTable(5, 5)
            };

            // act
            var newState = leftTarget.Process(state, new LeftCommand());

            // assert
            Assert.Equal(expected, newState.Position.Facing);
        }

        [Theory]
        [InlineData(Direction.NORTH, Direction.EAST)]
        [InlineData(Direction.EAST, Direction.SOUTH)]
        [InlineData(Direction.SOUTH, Direction.WEST)]
        [InlineData(Direction.WEST, Direction.NORTH)]
        public void Right_Command_Correctly_Changes_Direction(Direction currentDirection, Direction expected)
        {
            // arrange
            var state = new State
            {
                Position = new Position(1, 1, currentDirection),
                Table = new ZeroBoundedCartesianTable(5, 5)
            };

            // act
            var newState = rightTarget.Process(state, new RightCommand());

            // assert
            Assert.Equal(expected, newState.Position.Facing);
        }
    }
}
