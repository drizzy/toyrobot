﻿using Hooroo.ToyRobot.Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Hooroo.ToyRobot.Tests.Integration
{
    public class ToyRobotTests
    {
        private readonly MockTestOutput textOutput;
        private readonly ToyRobot target;

        public ToyRobotTests()
        {
            this.textOutput = new MockTestOutput();
            this.target = new ToyRobot(textOutput);
        }

        [Theory]
        [InlineData(@"PLACE 0,0,NORTH
MOVE
REPORT", new[] { "> 0,1,NORTH" })]
        [InlineData(
@"PLACE 0,0,NORTH
LEFT
REPORT",
           new[] { "> 0,0,WEST" })]
        [InlineData(@"PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT", new [] { "> 3,3,NORTH" })]
        [InlineData(@"PLACE 1,2,EAST
MOVE
MOVE
REPORT
LEFT
MOVE
REPORT", new[] { "> 3,2,EAST", "> 3,3,NORTH" })]
        [InlineData(@"MOVE
PLAICE34 11 555
LEFT
LEFT
RIGHT
MOVE
PLACE 6,4,WEST
REPORT
PLACE 1,2,EAST
MOVE
MOVE
REPORT
LEFT
MOVE
REPORT", new[] { "> 3,2,EAST", "> 3,3,NORTH" })]
        public void SampleData_Tests(string commands, string[] output)
        {
            // arrange

            // act
            target.ProcessCommands(commands);

            // assert
            Assert.Equal(output, textOutput.Output);

        }
    }
}
