﻿using Hooroo.ToyRobot.CommandHandlers;
using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using Hooroo.ToyRobot.Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Hooroo.ToyRobot.Tests
{
    public class CommandHandlerFactoryTests
    {
        private readonly MockTestOutput textoutput;
        private readonly PseudoStaticCommandHandlerFactory target;
       

        public CommandHandlerFactoryTests(ITestOutputHelper testOutputHelper)
        {
            this.textoutput = new MockTestOutput();
            this.target = new PseudoStaticCommandHandlerFactory(new ICommandHandler[]
            {
                new MoveCommandHandler(),
                new RightCommandHandler(),
                new LeftCommandHandler(),
                new ReportCommandHandler(textoutput),
                new PlaceCommandHandler()
            });
        }

        [Fact]
        public void CommandHandlerFactory_Gets_Correct_CommandHandler()
        {
            // arrange
            var command = new MoveCommand();

            // act
            var handler = target.GetCommandHandler(command);

            // assert
            Assert.IsType<MoveCommandHandler>(handler);
        }

        private class MockCommand :ICommand
        { }
        
        [Fact]
        public void CommandHandlerFactory_Returns_Null_For_UnknownType()
        {
            // arrange 
            var command = new MockCommand();

            // act
            var handler = target.GetCommandHandler(command);

            // assert
            Assert.Null(handler);
        }
    }
}
