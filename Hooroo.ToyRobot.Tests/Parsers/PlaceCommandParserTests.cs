﻿using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using Hooroo.ToyRobot.Parsers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Hooroo.ToyRobot.Tests
{
    public class PlaceCommandParserTests
    {
        ICommandParser<PlaceCommand> target;

        public PlaceCommandParserTests()
        {
            this.target = new PlaceCommandParser();
        }

        [Theory]
        [InlineData("PLACE 1,1,NORTH", 1, 1, Direction.NORTH)]
        [InlineData("PLACE 4,1,EAST", 4, 1, Direction.EAST)]
        [InlineData("PLACE 2,4,SOUTH", 2, 4, Direction.SOUTH)]
        [InlineData("PLACE 7,33,WEST", 7, 33, Direction.WEST)]
        [InlineData("PLACE -1,200,EAST", -1, 200, Direction.EAST)]
        public void ValidDataParsesToCorrectValues(string command, int expectedX, int expectedY, Direction expectedDirection)
        {
            // arrange

            // act
            var result = target.Parse(command);

            // assert
            Assert.Equal(expectedX, result.X);
            Assert.Equal(expectedY, result.Y);
            Assert.Equal(expectedDirection, result.Facing);
        }

        [Theory]
        [InlineData("PLACE abcdefghjik,1,NORTH", "abcdefghjik" )]
        [InlineData("PLACE 4,abcahhhhjjjj,EAST", "abcahhhhjjjj")]
        [InlineData("PLACE 2,4,DOGCAT", "DOGCAT")]
        [InlineData("PLACE 7,33", "is not a valid command")]
        [InlineData("P 1,2,3 4,5", "is not a valid command")]
        [InlineData("PLAICE -1,200,EAST", "is not a valid command")]
        public void InvalidData_Throws_Meaningful_Exception(string command, string substringHelpText)
        {
            // arrange

            // act
            var exception = Assert.Throws<ArgumentException>(() => target.Parse(command));

            // assert
            Assert.Contains(substringHelpText, exception.Message);
        }

    }
}
