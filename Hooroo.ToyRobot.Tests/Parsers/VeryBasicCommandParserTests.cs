using Hooroo.ToyRobot.Commands;
using Hooroo.ToyRobot.Interfaces;
using Hooroo.ToyRobot.Parsers;
using System;
using Xunit;

namespace Hooroo.ToyRobot.Tests
{
    public class VeryBasicCommandParserTests
    {
        private ICommandParser<LeftCommand> target;

        public VeryBasicCommandParserTests()
        {
            target = new LeftCommandParser();
        }

        [Fact]
        public void Parse_ValidInput_Returns_CorrectCommand()
        {
            // arrange            

            // act
            var result = target.Parse("LEFT");

            // assert
            Assert.IsType<LeftCommand>(result);
        }

        [Theory]
        [InlineData("left")]
        [InlineData("cow")]
        public void Parse_InvalidArgument_Throws_InvalidArgumentException(string input)
        {
            // arrange            

            // act
            Exception ex = Assert.Throws<ArgumentException>(() => target.Parse(input));

            // assert            
        }

        [Fact]
        public void Handle_ValidInput_Returns_True()
        {
            // arrange            

            // act
            var result = target.Handles("LEFT");

            // assert
            Assert.True(result);
        }

        [Theory]
        [InlineData("left")]
        [InlineData("sheep")]
        public void Handle_InvalidArgument_Returns_False(string input)
        {
            // arrange            

            // act
            var result = target.Handles(input);

            // assert            
            Assert.False(result);
        }
    }
}
