﻿using System;

namespace Hooroo.ToyRobot.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ToyRobot toyRobot = new ToyRobot(new ConsoleTextOutput());

            while (true)
            {
                var input = Console.ReadLine();
                toyRobot.ProcessCommands(input);
            }
        }
    }
}
